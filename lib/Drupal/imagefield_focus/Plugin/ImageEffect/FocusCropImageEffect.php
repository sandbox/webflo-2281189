<?php

/**
 * @file
 * Contains \Drupal\imagefield_focus\Plugin\ImageEffect\FocusCropImageEffect.
 */

namespace Drupal\imagefield_focus\Plugin\ImageEffect;

use Drupal\Core\Image\ImageInterface;
use Drupal\image\Annotation\ImageEffect;
use Drupal\image\Plugin\ImageEffect\CropImageEffect;
use Drupal\image\Plugin\ImageEffect\ResizeImageEffect;

/**
 * Crops an image resource.
 *
 * @ImageEffect(
 *   id = "imagefield_focus_crop",
 *   label = @Translation("Focus Crop"),
 *   description = @Translation("Crop based on data provided by <em>ImageField Focus</em>.")
 * )
 */
class FocusCropImageEffect extends ResizeImageEffect {

  public function applyEffect(ImageInterface $image) {
    $file = $this->loadFile($image);
    if (!isset($file)) {
      return;
    }

    $target = $this->configuration['target'];
    $crop_rect = isset($file->crop_rect) ? imagefield_focus_parse($file->crop_rect) : FALSE;
    $focus_rect = isset($file->focus_rect) ? imagefield_focus_parse($file->focus_rect) : FALSE;

    if ($crop_rect && ($target == 'crop_rect only' || $target == 'crop_rect first' || $target == 'focus_rect first' && !$focus_rect)) {
      if (!$image->crop($crop_rect['xoffset'], $crop_rect['yoffset'], $this->configuration['width'], $this->configuration['height'])) {
        watchdog('image', 'Image crop failed using the %toolkit toolkit on %path (%mimetype, %dimensions)', array(
          '%toolkit' => $image->getToolkitId(),
          '%path' => $image->getSource(),
          '%mimetype' => $image->getMimeType(),
          '%dimensions' => $image->getWidth() . 'x' . $image->getHeight()
        ), WATCHDOG_ERROR);
        return FALSE;
      }
    }
    if ($focus_rect && ($target == 'focus_rect only' || $target == 'focus_rect first' || $target == 'crop_rect first' && !$crop_rect)) {
      if (!$image->crop($focus_rect['xoffset'], $focus_rect['yoffset'], $this->configuration['width'], $this->configuration['height'])) {
        watchdog('image', 'Image crop failed using the %toolkit toolkit on %path (%mimetype, %dimensions)', array(
            '%toolkit' => $image->getToolkitId(),
            '%path' => $image->getSource(),
            '%mimetype' => $image->getMimeType(),
            '%dimensions' => $image->getWidth() . 'x' . $image->getHeight()
          ), WATCHDOG_ERROR);
        return FALSE;
      }
    }
    return TRUE;
  }


  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();
    $config['target'] = 'crop_rect only';
    return $config;
  }

  public function getForm() {
    $form = parent::getForm();

    $form['target'] = array(
      '#type' => 'radios',
      '#title' => t('Crop based on'),
      '#options' => array(
        'crop_rect only' => t('Crop rectangle only'),
        'focus_rect only' => t('Focus rectangle only'),
        'crop_rect first' => t('Crop rectangle if available, Focus rectangle otherwise'),
        'focus_rect first' => t('Focus rectangle if available, Crop rectangle otherwise'),
      ),
      '#default_value' => $this->configuration['target']
    );

    return $form;
  }

  /**
   * Get the file entity based on the URI.
   *
   * @param ImageInterface $image
   * @return bool|\Drupal\file\FileInterface
   */
  protected function loadFile(ImageInterface $image) {
    // Get the file record based on the URI. If not in the database just return.
    /** @var \Drupal\file\FileInterface[] $files */
    $files = entity_load_multiple_by_properties('file', array('uri' => $image->getSource()));
    if (count($files)) {
      foreach ($files as $item) {
        // Since some database servers sometimes use a case-insensitive comparison
        // by default, double check that the filename is an exact match.
        if ($item->getFileUri() === $image->getSource()) {
          return $item;
        }
      }
    }
    return FALSE;
  }

}
