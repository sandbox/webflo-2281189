<?php

/**
 * @file
 * Contains \Drupal\imagefield_focus\Plugin\ImageEffect\FocusScaleAndCropImageEffect.
 */

namespace Drupal\imagefield_focus\Plugin\ImageEffect;

use Drupal\Core\Image\ImageInterface;
use Drupal\image\ConfigurableImageEffectInterface;
use Drupal\image\ImageEffectBase;
use Drupal\image\Plugin\ImageEffect\ScaleAndCropImageEffect;

/**
 * Scales an image resource.
 *
 * @ImageEffect(
 *   id = "imagefield_focus_scale_and_crop",
 *   label = @Translation("Focus Scale And Crop"),
 *   description = @Translation("Scale and crop based on data provided by <em>ImageField Focus</em>.")
 * )
 */
class FocusScaleAndCropImageEffect extends ScaleAndCropImageEffect {

  /**
   * {@inheritdoc}
   */
  public function applyEffect(ImageInterface $image) {
    $file = $this->loadFile($image);
    if (!isset($file)) {
      return;
    }

    $xoffset = 0;
    $yoffset = 0;

    if (isset($file->crop_rect) && $rect = imagefield_focus_parse($file->crop_rect)) {
      if (!$image->crop($rect['x'], $rect['y'], $this->configuration['width'], $this->configuration['height'])) {
        return FALSE;
      }
      $xoffset = $rect['xoffset'];
      $yoffset = $rect['yoffset'];
    }

    if (isset($file->focus_rect) && $rect = imagefield_focus_parse($file->focus_rect)) {
      $rect['xoffset'] -= $xoffset;
      $rect['yoffset'] -= $yoffset;

      $scale = min(1, $this->configuration['width'] / $rect['width'], $this->configuration['height'] / $rect['height']);
      $scale_low = max($this->configuration['width'] / $image->getWidth(), $this->configuration['height'] / $image->getHeight());
      if ($scale < $scale_low) {
        if (!empty($this->configuration['maintain_size'])) {
          $scale = $scale_low;
        }
        else {
          $scale_low = $scale;
        }
      }
      elseif ($this->configuration['strength'] == 'medium') {
        $scale = sqrt($scale * $scale_low);
      }
      elseif ($this->configuration['strength'] == 'low') {
        $scale = $scale_low;
      }

      $width0 = $image->getWidth() * $scale;
      $height0 = $image->getHeight() * $scale;
      if ($scale != 1 && !$image->resize($width0, $height0)) {
        return FALSE;
      }

      foreach ($rect as $key => $value) {
        $rect[$key] = $value * $scale;
      }
      $width = min($width0, $this->configuration['width']);
      $height = min($height0, $this->configuration['height']);
      $xoffset = min($width0 - $width, max(0, $rect['xoffset'] + ($rect['width'] - $width) / 2));
      $yoffset = min($height0 - $height, max(0, $rect['yoffset'] + ($rect['height'] - $height) / 2));
      if (!$image->crop(round($xoffset), round($yoffset), $width, $height)) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();
    $config['maintain_size'] = FALSE;
    $config['strength'] = 'medium';
    $config['fallback'] = 'image';
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function getForm() {
    $form = parent::getForm();

    $form['maintain_size'] = array(
      '#type' => 'checkbox',
      '#title' => t('Fixed dimensions'),
      '#description' => t('Select this to force the width and height to always match the above values. This may cause upscaling, and in unusual cases may also result in parts of the focus area not being included in the image.'),
      '#default_value' => $this->configuration['maintain_size'],
    );

    $form['strength'] = array(
      '#type' => 'radios',
      '#title' => t('Focus strength'),
      '#options' => array(
        'high' => t('High'),
        'medium' => t('Medium'),
        'low' => t('Low'),
      ),
      '#default_value' => $this->configuration['strength'],
    );

    if (module_exists('smartcrop')) {
      $form['fallback'] = array(
        '#type' => 'radios',
        '#title' => t('Fallback effect'),
        '#options' => array(
          'image' => t('!module: !effect', array(
              '!module' => 'Image',
              '!effect' => t('Scale And Crop')
            )),
          'smartcrop' => t('!module: !effect', array(
              '!module' => 'SmartCrop',
              '!effect' => t('Scale and Smart Crop')
            )),
        ),
        '#default_value' => $this->configuration['fallback'],
        '#description' => t('The effect to apply when no focus data is available.'),
      );
    }

    return $form;
  }

  /**
   * Get the file entity based on the URI.
   *
   * @param ImageInterface $image
   * @return bool|\Drupal\file\FileInterface
   */
  protected function loadFile(ImageInterface $image) {
    // Get the file record based on the URI. If not in the database just return.
    /** @var \Drupal\file\FileInterface[] $files */
    $files = entity_load_multiple_by_properties('file', array('uri' => $image->getSource()));
    if (count($files)) {
      foreach ($files as $item) {
        // Since some database servers sometimes use a case-insensitive comparison
        // by default, double check that the filename is an exact match.
        if ($item->getFileUri() === $image->getSource()) {
          return $item;
        }
      }
    }
    return FALSE;
  }

}
